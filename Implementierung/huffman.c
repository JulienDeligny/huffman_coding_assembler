#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>
extern int huffman_encode(char*,char*,unsigned int);
extern int huffman_decode(char*,char*,unsigned int);

void command(){
	printf("syntax:\n-d\n-i <Input Path(input.dat per default)> \n-o <Output Path(output.dat per default)>\n-s <positiv integer result_size(INT_MAX per default)>\n");
	exit(-1);
}


int main(int argc,char** argv){


	//checking arguments of the main method

unsigned int resultsize=UINT_MAX-8; 		//result_size set to mac_int per default
 	int (* function)(char*,char*,unsigned int)=&huffman_encode,		//function set to encode per default
 		optchecker=0;			//checker to avoid errors in options
char option 			
,* input="input.dat" 			//input file set to "input.dat" per default
,* output="output.dat" 			//output file set to "output.dat" per default
, * checknr; 					//used to check whether the optional size was correct

while((option=getopt(argc,argv,"di:o:s:"))!=-1)
switch(option){
case 'd':if (optchecker&1) command();function=&huffman_decode;optchecker|=1;break;
case 'i':if (optchecker&2) command();input=optarg;optchecker|=2;break;
case 'o':if (optchecker&4) command();output=optarg;optchecker|=4;break;
case 's':if (optchecker&8) command();resultsize=strtol(optarg,&checknr,0);if (*checknr||resultsize<0)command();optchecker|=8;break;
default:command();
}


struct stat sb;

if (stat(input, &sb) < 0) {

	printf("%s not found\n",input);
		exit(-1);
}
	int size= sb.st_size;

	if (size>UINT_MAX-5) 		///check if the whole data size can be handled by the function
	{
		printf("%s is too large\n",input);
		exit(-1);
	}

	char* data=calloc(size+5,sizeof(char)); 		//the data being read
	unsigned int *data2=(unsigned int *)data;							//int* to set the size at the begining
	FILE* in=fopen(input,"rb+"),*out; 				//opening input file

	if(!in) {

		printf("%s not found\n",input);
		exit(-1);
	}

	*data2=fread(data+4,sizeof(char),size,in); 			//reading from the input data while setting its size at the beginning of the data
	
		if	(ferror(in)){

		printf("Problem with %s!\n",input);
		exit(-1);
	}

fclose(in);

char* result=calloc(resultsize+8,1);		

int r=function(data,result,resultsize);				//principal function

free(data); 										//data not needed anymore

if (r<0){
	if (optchecker&1)
		printf("An error has occured. Make sure that the input file has already been encoded by this program. If an output size has been suggested, it could be not adequate.\n");
	else
		printf("An error has occured. Make sure that the input file is entierly ascii encoded. If an output size has been suggested, it could be not adequate.\n");
	exit(-1);
}
printf("result:%d byte\n",r); 						//displaying the size of the result

out=fopen(output,"w+"); 							//opening output file

	if (!out) {
		printf("There is a problem with %s!\n",output);
		exit(-1);
	}

fwrite(result,r,sizeof(char),out); 				//writing into the output file

	if(ferror(out))
{

		printf("There is a Problem with %s!\n",output);
		exit(-1);
	}

fclose(out);

free(result);
}

